import { useState } from 'react'
import passGenerate from './components/Pass'
import './App.scss'
import { FaRegClipboard } from "react-icons/fa";

const App = () => {
  const [result, setResult] = useState('')
  const [inputVal, setInputVal] = useState('')

  const handle_clipboard = () => {
    
  }

  const handleChange = (event) => {
    const { value } = event.target
  
    if (value < 0 && value > 100) return null 
  
    setInputVal(prevVal => value)

    passGenerate(inputVal, result, setResult)
  }

  return (
    <div className='container'>
      <h1 className='container__title'>Password Generator</h1>
      <div className="container__inputs">
        <input className="container__inputRange" type="range" min={0} max={100} value={inputVal} onChange={handleChange}/>
        <input className="container__inputNum" type="number" value={inputVal} />
      </div>
      <div className="container__inputs2">
        <input type="text" className="container__result" value={ result }/>
        <button className="container__button" onClick={() => {
          navigator.clipboard.writeText(result); 
          handle_clipboard();
          }}>
            <FaRegClipboard className='container__button_icon'/>
        </button>
      </div>
    </div>
  )
}

export default App